#pragma once
#define N 996	//FIXME dodawanie z parametrow

class MatrixNxM
{
public:
	MatrixNxM();
	MatrixNxM(int n, int m);
	~MatrixNxM();

protected:
	int rows;
	int columns;
	float** matrix;
	void clearMatrix();
	void copyMatrix(float** srcMatrix);
};


// SetsOfEquations.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DiagonaMatrix.h"
#include "Set.h"
#include <iostream>
#include <fstream>
#include <time.h>

#define N 996
#define F 5
#define E 2

using namespace std;

int main(char* argv, int argc)
{
	float a1 = 5 + E;
	float a2 = -1;
	float a3 = -1;

	float values[3] = { a1, a2, a3 };
	DiagonalMatrix* A = new DiagonalMatrix(N, values);
	A->fillDiagonal();
	//A->showMatrix();

	Set setEquations(*A, N);
	//setEquations.showA();

	cout << "METODA JACOBIEGO" << endl;
	clock_t startJacobi = clock();
	int jacobiIterations = setEquations.checkIterationsJacobi();
	clock_t stopJacobi = clock();
	double elapsedJacobiTime = (double)(stopJacobi - startJacobi) * 1000.0 / CLOCKS_PER_SEC;
	cout << "iteracji:	" << jacobiIterations << endl;
	cout << "czas:		" << elapsedJacobiTime << " ms" << endl;
	//setEquations.writeResidum();


	cout << "METODA GAUSSA-SEIDLE'A" << endl;
	clock_t startGS = clock();
	int GSIterations = setEquations.checkIterationsGauss_Seidl();
	clock_t stopGS = clock();
	double elapsedGSTime = (double)(stopGS - startGS) * 1000.0 / CLOCKS_PER_SEC;
	cout << "iteracji:	" << GSIterations << endl;
	cout << "czas:		" << elapsedGSTime << " ms" << endl;
	//setEquations.writeResidum();
	delete A;


	a1 = 3;
	a2 = -1;
	a3 = -1;
	float values2[3] = { a1, a2, a3 };
	DiagonalMatrix* C = new DiagonalMatrix(N, values2);
	C->fillDiagonal();
	Set setEquations2(*C, N);
	cout << "=========== ZADANIE C ==================" << endl;
	//cout << "Jacobi" << endl;
	//jacobiIterations = setEquations2.checkIterationsJacobi();
	//cout << "iteracje: " << jacobiIterations << endl;
	//cout << "Gaus-Seidl" << endl;
	//GSIterations = setEquations2.checkIterationsGauss_Seidl();
	//cout << "iteracje: " << GSIterations << endl;
	cout << "odp: metody nie zbiegaja sie" << endl;


	cout << "=========== ZADANIE D =================" << endl;
	vector<float> LUresiduum;
	float normLU = 0.0;
	//LUresiduum = setEquations2.doLuFactorization(values2);
	//normLU = setEquations2.calculateNorm(LUresiduum);
	cout << "Norma residuum macierzy: " << normLU << endl;
	delete C;
	ofstream exEOut;
	exEOut.open("zadE.txt");
	cout << "=========== ZADANIE E ===================" << endl;
	cout << "N	Jacobi		Gaus-Seidel		Faktoryzacja LU" << endl;
	exEOut << "N	Jacobi		Gaus-Seidel		Faktoryzacja LU" << endl;
	int nValues[7] = { 100, 500, 1000, 2000, 3000, 4000, 5000 };

	double elapsedLUTime;
	clock_t startLU;
	clock_t stopLU;
	for (int i = 0; i < 7; i++)
	{
		DiagonalMatrix* matrixE = new DiagonalMatrix(nValues[i], values2);
		matrixE->fillDiagonal();

		Set setEquationsE(*matrixE, nValues[i]);

		startJacobi = clock();
		jacobiIterations = setEquationsE.checkIterationsJacobi();
		stopJacobi = clock();
		elapsedJacobiTime = (double)(stopJacobi - startJacobi) * 1000.0 / CLOCKS_PER_SEC;

		startGS = clock();
		GSIterations = setEquationsE.checkIterationsGauss_Seidl();
		stopGS = clock();
		double elapsedGSTime = (double)(stopGS - startGS) * 1000.0 / CLOCKS_PER_SEC;

		startLU = clock();
		LUresiduum = setEquationsE.doLuFactorization(values);
		stopLU = clock();
		elapsedLUTime = (double)(stopLU - startLU) * 1000.0 / CLOCKS_PER_SEC;

		cout << nValues[i] << "		" << elapsedJacobiTime << "		" << elapsedGSTime << "		" << elapsedLUTime << endl;
		exEOut << nValues[i] << "		" << elapsedJacobiTime << "		" << elapsedGSTime << "		" << elapsedLUTime << endl;
		delete matrixE;
	}
	exEOut.close();

	return 0;
}


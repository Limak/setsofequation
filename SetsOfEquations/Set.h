#pragma once
#include "DiagonaMatrix.h"
#include <vector>

using namespace std;
class Set
{
public:
	Set();
	Set(DiagonalMatrix inputA, int n);
	void showA();
	int checkIterationsJacobi();
	int checkIterationsGauss_Seidl();
	vector<float> doLuFactorization(float paramsU[3]);
	void writeResidum();
	void writeResiduum(vector<float> vec);
	float calculateNorm(vector<float> vecToNorm);
	~Set();

private:
	float* b;
	float* x;	//rozwiazania ukladu rownan
	float* negatedAii;
	float* normResidum;
	DiagonalMatrix A;
	int setEquationsSize;
	void createNetadeAii();
	vector<float> forwardSubstitution(DiagonalMatrix* L);
	vector<float> backwardSubstitution(DiagonalMatrix* U, vector<float> y);
};


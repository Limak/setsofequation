#pragma once
#include "MatrixNxM.h"
#include <fstream>

using namespace std;

class DiagonalMatrix : public MatrixNxM
{
public:
	DiagonalMatrix();
	DiagonalMatrix(int n);
	DiagonalMatrix(int n, float params[3]);
	void fillFromSelected(int beginningRow, int beginningColumn, float value);
	void fillDiagonal();
	void showMatrix();
	float** getMatrix();
	void setValue(int row, int column, float value);
	float** getL();
	float** getU();
	float** getD();
	~DiagonalMatrix();

private:
	float a1;
	float a2;
	float a3;
	float** lowTriangle;
	float** upperTriangle;
	float** diagonalTriangle;
	void createLDU();
};


#include "Set.h"
#include <math.h>
#include <iostream>

using namespace std;


Set::Set()
{
}

Set::Set(DiagonalMatrix inputA, int n)
{
	b = new float[n];
	x = new float[n];
	negatedAii = new float[n];
	setEquationsSize = n;
	normResidum = new float[setEquationsSize];
	A = inputA;
	for (int i = 0; i < setEquationsSize; i++)
	{
		b[i] = sin(i*(5 + 1));
		x[i] = 0;
	}
	createNetadeAii();
}

void Set::showA()
{
	A.showMatrix();
}


//x1-> x[k]
//x2-> x[k+1]
int Set::checkIterationsJacobi()
{
	float residum = pow(10, -9);
	int iterationsAmount = 0;
	float sum = 1;
	float* normResidum = new float[setEquationsSize];
	float* x2 = new float[setEquationsSize];
	float* x1 = new float[setEquationsSize];

	for (int i = 0; i < setEquationsSize; i++)
	{
		x1[i] = 0;
		x2[i] = 0;
	}

	while (sum >= residum)
	{
		sum = 0;
		for (int i = 0; i < setEquationsSize; i++)
		{
			float temp = 0;

			for (int j = 0; j < setEquationsSize; j++)
			{
				normResidum[i] = 0;
			}

			x2[i] = b[i];
			//suma do diagonali
			for (int j = 0; j < i; j++)
			{
				temp += A.getMatrix()[i][j] * x1[j];
			}

			x2[i] -= temp;
			temp = 0;

			//suma podiagonali
			for (int j = i + 1; j < setEquationsSize; j++)
			{
				temp += A.getMatrix()[i][j] * x1[j];
			}

			x2[i] -= temp;
			x2[i] *= negatedAii[i];

			normResidum[i] = x1[i] - x2[i];
		}

		//aby w nastepnej iteracji miec x[k]
		for (int i = 0; i < setEquationsSize; i++)
		{
			sum += abs(normResidum[i]);
			x1[i] = x2[i];
		}
		//cout << sum << "	";
		iterationsAmount++;
	}
	delete[] x1;
	delete[] x2;
	//delete[] normResidum;
	return iterationsAmount;
}


//x1->x[k+1]
//x->x[k]
int Set::checkIterationsGauss_Seidl()
{
	float residum = pow(10, -9);
	float sum = 1;
	//float* normResidum = new float[setEquationsSize];
	float* x1 = new float[setEquationsSize];
	int iterationsAmount = 0;

	for (int i = 0; i < setEquationsSize; i++)
	{
		x1[i] = 0;
	}

	while (sum >= residum)
	{
		sum = 0;

		for (int i = 0; i < setEquationsSize; i++)
		{
			x[i] = b[i] * negatedAii[i];

			//do diagonali
			for (int j = 0; j < i; j++)
			{
				x[i] -= A.getMatrix()[i][j] * negatedAii[j] * x[j];
				float tempXL = x[i];
			}
			//od diagonali do konca macierzy
			for (int j = i + 1; j < setEquationsSize; j++)
			{
				x[i] -= A.getMatrix()[i][j] * negatedAii[j] * x[j];
				float tempXU = x[i];
			}

			normResidum[i] = x[i] - x1[i];
			//cout << normResidum[i] << "	";
			float tempResidum = normResidum[i];
		}


		//przypisanie x do x1 aby aktualne x[k+1] w kolejnym obiegu bylo x[k]
		for (int i = 0; i < setEquationsSize; i++)
		{
			x1[i] = x[i];
			float tempSum = abs(normResidum[i]);
			sum += abs(normResidum[i]);
		}
		//cout << sum << "	";
		iterationsAmount++;
	}
	//cout << endl;
	//delete[] normResidum;
	delete[] x1;
	return iterationsAmount;
}

vector<float> Set::doLuFactorization(float paramsU[3])
{
	float paramsL[3] = { 1, 0, 0 };
	DiagonalMatrix* L = new DiagonalMatrix(setEquationsSize, paramsL);
	DiagonalMatrix* U = new DiagonalMatrix(setEquationsSize, paramsU);	//L=A

	//LU dekompozycja
	for (int i = 0; i < setEquationsSize; i++)
	{
		L->setValue(i, i, 1);
	}

	for (int i = 0; i < setEquationsSize; i++)
	{
		for (int j = i; j < setEquationsSize; j++)
		{
			float sum = 0.0;
			for (int k = 0; k < i; k++)
			{
				float tempL = L->getMatrix()[i][k];
				float tempU = U->getMatrix()[k][j];
				sum += L->getMatrix()[i][k] * U->getMatrix()[k][j];
			}
			float tempValue= A.getMatrix()[i][j] - sum;
			U->setValue(i, j, tempValue);
		}


		for (int j = i + 1; j < setEquationsSize; j++)
		{
			float sum = 0.0;
			for (int k = 0; k < i; k++)
			{
				sum += L->getMatrix()[j][k] * U->getMatrix()[k][i];
			}
			float tempValue = (1 / U->getMatrix()[i][i])*(A.getMatrix()[j][i] - sum);
			L->setValue(j, i, tempValue);
		}
	}

	vector<float> y = forwardSubstitution(L);
	vector<float> x = backwardSubstitution(U, y);
	delete L;
	delete U;
	return x;

}

vector<float> Set::forwardSubstitution(DiagonalMatrix* L)
{
	vector<float> y(setEquationsSize);
	float tempB0 = b[0];
	y[0] = b[0];
	for (int i = 1; i < setEquationsSize; i++)
	{
		float sum = 0.0;

		for (int j = 0; j < i; j++)
		{
			sum += L->getMatrix()[i][j] * y[j];
		}
		float tempY = 1 / L->getMatrix()[i][i] * (b[i] - sum);
		y[i] = 1 / L->getMatrix()[i][i] * (b[i] - sum);
	}
	return y;
}

vector<float> Set::backwardSubstitution(DiagonalMatrix* U, vector<float> y)
{
	vector<float> x(setEquationsSize);
	int n = setEquationsSize;
	x[n - 1] = y[n - 1] / U->getMatrix()[n - 1][n - 1];

	for (int i = n - 2; i >= 0; i--)
	{
		float sum = 0.0;

		for (int j = n - 1; j > i; j--)
		{
			sum += U->getMatrix()[i][j] * x[j];
		}
		float tempX = (y[i] - sum) / U->getMatrix()[i][i];
		x[i] = (y[i] - sum) / U->getMatrix()[i][i];
	}
	return x;
}

void Set::writeResidum()
{
	for (int i = 0; i < setEquationsSize; i++)
	{
		cout << normResidum[i] << "	";
	}
	cout << endl << endl;
}

void Set::writeResiduum(vector<float> vec)
{
	for (auto &x : vec)
	{
		cout << x << " ";
	}
}

float Set::calculateNorm(vector<float> vecToNorm)
{
	float norm = 0.0;

	for (auto &x : vecToNorm)
	{
		norm += x*x;
	}
	return sqrtf(norm);
}

Set::~Set()
{
	delete[] b;
	delete[] x;
	//delete[] normResidum;
}

void Set::createNetadeAii()
{
	for (int i = 0; i < setEquationsSize; i++)
	{
		negatedAii[i] = (float)pow(A.getMatrix()[i][i], -1);
	}
}


